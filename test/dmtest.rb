# Test that Datamapper and the database things are working
#
require 'data_mapper'

load File.expand_path('../dm/currency-1.0.0.dm',__FILE__)
load File.expand_path('../dm/currency_group-1.0.0.dm',__FILE__)

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, 'oracle://nick:secret@nixora.local/orcl')
DataMapper.finalize

DataMapper.auto_migrate!

americas = CurrencyGroup.create(
  :code => 'Americas',
  :description => 'The Americas',
  :is_commodity => true
)
europe = CurrencyGroup.create(
  :code => 'Europe',
  :description => 'European Union',
  :is_commodity => false
)
europe.currencies.create(
  :code	=> 'GBP',
  :description => 'Pounds Sterling',
  :exchange_rate => 1.5,
  :is_designated => false
)
europe.currencies.create(
  :code	=> 'EUR',
  :description => 'Euro',
  :exchange_rate => 1.2,
  :is_designated => false
)
americas.currencies.create(
  :code	=> 'USD',
  :description => 'Greenback',
  :exchange_rate => 1.0,
  :is_designated => true
)
