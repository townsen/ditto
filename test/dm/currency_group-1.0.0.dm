# This class encapsulates the existing Currency Group database
#
class CurrencyGroup
  VERSION = '1.0.0'
  include DataMapper::Resource
  property  :code,	  String,   :key => true
  property  :description, Text,	    :required => true, :lazy => false
  property  :is_commodity,Boolean,  :default => 0
  has n, :currencies
end
