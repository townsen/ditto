# This class encapsulates the existing Currency database
#
class Currency
  VERSION = '1.0.0'
  include DataMapper::Resource

  property  :code,	      String,	:key => true
  property  :description,     Text,	:required => true, :lazy => false
  property  :exchange_rate,   Float,	:default => 0.0
  property  :is_designated,   Boolean,	:default => 0

  belongs_to :currency_group
end
