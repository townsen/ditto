Database Independent Test Objects (Ditto)
-----------------------------------------

Ditto defines a simple DSL that allows test data to be expressed in a database independent format. The principal benefit being that when the underlying tables are remapped the QA team don't have to go and change all the historical test cases.

There are four parts:
1. The test instance data (in a typeless YAML form)
2. The entity definitions for the above (in a DSL)
3. A set of mappings for each entity to (multiple versions of) the database
4. The database declarations (expressed as Datamapper objects)

The idea was to replace an XML dialect (see sample at `test/data/simple_object.xml`) with something simpler yet more capable.

Notes on DSL
------------

The entity declaration style uses verb and hash only, without a block.
Contrast this with mapping style which uses verb, params and then block. Of course
mappings need code and so the block usage is natural. This could also be done
for the declarations, very similar to the way the gem 'datamapper' does it.

Versioning
----------

In Ditto all four of the parts are versioned using the [Semantic Versioning](http://semver.org) convention which can be summarized as:

PATCH 0.0.x level changes for implementation level detail changes, such as small bug fixes
MINOR 0.x.0 level changes for any backwards compatible API changes, such as new functionality/features
MAJOR x.0.0 level changes for backwards incompatible API changes, such as changes that will break existing users code if they update

Expressing Constraints
----------------------

We use the syntax and features of Gem:

* `'>= 2.2.0'` Is an optimistic constraint (3.x may break it)
* `['>= 2.2.0', '< 3.0']` is pessimistic - it imagines 3.x will break it.
* `'~> 2.2'` is a shortcut for the above (Note dropped PATCH level!)

Allowed Version Combinations
----------------------------

These are expressed using the following abbreviations:
* T - the test instance version
* E - the test entity version
* E:M - the mapping versions for an entity
* D - the database

The allowed version combinations are determined as follows:
* The version of the test instance data determines the entity selected. By convention the most recent entity with the same major version will be used. (This behaviour could be changed by adding an optional version requirement to the Entity if necessary)
* The version of the database objects is fixed at the start of the run by the datamart file.
* The actual loaded object versions are used to find the most-recent mapping whose database object version constraints are met.

It's best to look at the code!

Nick Townsend, Jan 2013
