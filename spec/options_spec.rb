require_relative '../lib/ditto/options'
require 'fileutils'
require 'tmpdir'

module Ditto
  describe Options do
    before(:all) do
      @tempdir = Dir.tmpdir
      @file1 = "#{@tempdir}/file1"
      @file2 = "#{@tempdir}/file2"
      FileUtils.touch [@file1, @file2]
#      ENV.delete 'DITTO_MART'
    end
    after(:all) do
      FileUtils.rm_f [@file1, @file2]
    end
    context "specifying no options" do
      it "should return defaults" do
	opts = Ditto::Options.new([])
	opts.connstring.should == Ditto::Options::DEFAULTS[:connstring]
	opts.dittomart.should == Ditto::Options::DEFAULTS[:dittomart]
	opts.entitydir.should == Ditto::Options::DEFAULTS[:entitydir]
	opts.verbose.should == 0
	opts.debug.should be_false
	opts.droptables.should be_false
	opts.loadfiles.size.should == 0
      end
      it "should return single path" do
	opts = Ditto::Options.new([@file1])
	opts.connstring.should == Ditto::Options::DEFAULTS[:connstring]
	opts.dittomart.should == Ditto::Options::DEFAULTS[:dittomart]
	opts.verbose.should == 0
	opts.debug.should be_false
	opts.droptables.should be_false
	opts.loadfiles.should == [@file1]
      end
    end
    context "specifying a dittomart" do
      it "should return it" do
	opts = Ditto::Options.new(["-m", @file1])
	opts.dittomart.should == @file1
      end
    end
    context "specifying a dittomart that doesn't exist" do
      it "should fail" do
	expect {
	  opts = Ditto::Options.new(["-m", "zz/zz/zz"])
	}.to raise_error(Errno::ENOENT)
      end
    end
    context "not specifying a dittomart" do
      it "should default to the environment" do
	pending "Can't work out how to set ENV within an rspec test"
	ENV['DITTO_MART'] = @file1
	opts = Ditto::Options.new([@file2])
	opts.dittomart.should == @file1
      end
    end
    context "specifying a connection string" do
      it "should return it" do
	mydb = "username/password@//myserver:1521/my.service.com"
	opts = Ditto::Options.new(["-c", "#{mydb}"])
	opts.connstring.should == mydb
      end
    end
    context "specifying files and no connection string" do
      it "should return the files" do
	opts = Ditto::Options.new([@file1, @file2])
	opts.loadfiles.should == [@file1, @file2]
      end
    end
    context "specifying files and a connection string" do
      it "should return the files" do
	opts = Ditto::Options.new(["-c", "u/p@//serv:1521/mydb", @file1, @file2])
	opts.loadfiles.should == [@file1, @file2]
      end
    end
    context "specifying droptables" do
      it "should set the options" do
	opts = Ditto::Options.new(["--droptables"])
	opts.droptables.should be_true
      end
    end
    context "specifying verbose" do
      it "should set verbose flag" do
	opts = Ditto::Options.new(["-v"])
	opts.verbose.should == 1
      end
      it "should set verbose flag twice" do
	opts = Ditto::Options.new(["-vv"])
	opts.verbose.should == 2
      end
    end
  end
end
