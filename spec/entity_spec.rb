require_relative '../lib/ditto/entity'

module Ditto
  describe Entity do
    context "When loading a ditto file" do
      before(:each) do
	Ditto::Entity.reset
      end
      it "should parse correct format" do
	Ditto::Entity.load_from_file("spec/fixtures/good.ditto").should be_true
      end
      it "should parse correct multiple version format" do
	Ditto::Entity.load_from_file("spec/fixtures/multiversion.ditto").should be_true
      end
      it "should not load duplicate multiple version format" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/dupversion.ditto").should be_true
	}.to raise_error Ditto::Error, /previously defined/
      end
      it "should not load the same one twice" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/good.ditto").should be_true
	  Ditto::Entity.load_from_file("spec/fixtures/good.ditto").should be_true
	}.to raise_error Ditto::Error, /previously defined/
      end
      it "should parse a syntax error" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/syntax.ditto")
	}.to raise_error Ditto::Error, /SyntaxError/
      end
      it "should identify a comma error" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/nocomma.ditto")
	}.to raise_error Ditto::Error, /missing map/
      end
      it "should identify an invalid mapping" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/badmap.ditto")
	}.to raise_error Ditto::Error, /missing block/
      end
      it "should identify a bad data map requirement" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/baddmrequire.ditto")
	}.to raise_error Ditto::Error, /Illformed requirement/
      end
      it "should parse a method typo" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/nomethod.ditto")
	}.to raise_error Ditto::Error, /NameError/
      end
      it "should identify a bad version" do
	expect {
	  Ditto::Entity.load_from_file("spec/fixtures/badver.ditto")
	}.to raise_error Ditto::Error, /Malformed version/
      end
    end
    context "Checking dependencies" do
      before (:each) do
	Ditto::Entity.reset
	Ditto::Entity.load_from_file("spec/fixtures/currency.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/currency_group.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/country.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/continent.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/random.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/circle.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/bigcircle.ditto")
	Ditto::Entity.load_from_file("spec/fixtures/dot.ditto")
      end
      it "should return empty dependency" do
	Ditto::Entity.dep_list(:currency_group, [], []).should == [:currency_group]
      end
      it "should return simple dependency" do
	Ditto::Entity.dep_list(:currency, [], []).should == [:currency_group, :currency]
      end
      it "should return multilevel dependency" do
	Ditto::Entity.dep_list(:country, [], []).should == [:currency_group, :currency, :country]
      end
      it "should return square dependency" do
	Ditto::Entity.dep_list(:continent, [], []).should == [:currency_group, :currency, :country, :continent]
      end
      it "should detect missing dependency" do
	expect {
	  Ditto::Entity.dep_list(:random, [], [])
	}.to raise_error "missing dependency: stealth"
      end
      it "should detect circular dependency" do
	expect {
	  Ditto::Entity.dep_list(:circle, [], [])
	}.to raise_error "circular dependency: circle"
      end
      it "should detect bigcircular dependency" do
	expect {
	  Ditto::Entity.dep_list(:bigcircle, [], [])
	}.to raise_error "circular dependency: bigcircle"
      end
    end
  end
end
