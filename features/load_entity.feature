@pending
Feature: Ditto loads simple entities
In order to create test data
As a user of Ditto
I want to load data definitions

Scenario: Ditto loads a simple entity
  Given a file named "simple_entity.yaml" with:
  """
  version: 1.0.0
  date:    2012-10-13
  ---
  currency:
    code: GBP
    description: Pounds Sterling
    exchange_rate: 1.5
    is_designated: false
    currency_group: Europe
  ---
    currency_group:
      code: Europe
      description: European Countries
      is_commodity: false
  """
  When I run ditto on "simple_entity.yaml"
  Then the exit code should be 0
  And the stdout should be
  """
  validated 2 entities, 2 instances

  """
  When I run ditto -v on "simple_entity.yaml"
  Then the exit code should be 0
  And the stdout should be
  """
  1 data files, 2 instances loaded, 0 errors
  checking instances...
  validated 2 entities, 2 instances

  """
  When I run ditto -vv on "simple_entity.yaml"
  Then the exit code should be 0
  Then the stdout should be
  """
  loading file: simple_entity.yaml
  1 data files, 2 instances loaded, 0 errors
  checking instances...
  currency
  {"code"=>"GBP", "description"=>"Pounds Sterling", "exchange_rate"=>1.5, "is_designated"=>false, "currency_group"=>"Europe"}
  currency_group
  {"code"=>"Europe", "description"=>"European Countries", "is_commodity"=>false}
  validated 2 entities, 2 instances

  """
