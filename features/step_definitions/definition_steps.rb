Given /^a file named "(.*?)" with:$/ do |arg1, string|
  filename = "#{@tmpdir}/#{arg1}"
  File.open(filename,"w") { |f| f.write string }
  @files[arg1] = filename
end

Then /^the exit code should be (\d+)$/ do |arg1|
  @rc.should == arg1.to_i
end

Then /^the stdout should be$/ do |string|
  @out.should == string
end

When /^I run ditto on "(.*?)"$/ do |arg1|
  @out = `ruby -I lib bin/ditto #{@files[arg1]}`
  raise "Exec failed!" if $?.success?.nil?
  @rc = $?.exitstatus
end

When /^I run ditto on two files "(.*?)", "(.*?)"$/ do |arg1,arg2|
  @out = `ruby -I lib bin/ditto --droptables -d #{@files[arg1]} #{@files[arg2]}`
  raise "Exec failed!" if $?.success?.nil?
  @rc = $?.exitstatus
end

When /^I run ditto \-v on "(.*?)"$/ do |arg1|
  @out = `ruby -I lib bin/ditto -v #{@files[arg1]}`
  raise "Exec failed!" if $?.success?.nil?
  @rc = $?.exitstatus
end

When /^I run ditto \-vv on "(.*?)"$/ do |arg1|
  @out = `ruby -I lib bin/ditto -vv #{@files[arg1]}`
  raise "Exec failed!" if $?.success?.nil?
  @rc = $?.exitstatus
end

When /^I run ditto with the file "(.*?)" twice$/ do |arg1|
  @out = `ruby -I lib bin/ditto #{@files[arg1]} #{@files[arg1]}`
  raise "Exec failed!" if $?.success?.nil?
  @rc = $?.exitstatus
end
