@pending
Feature: Ditto parses simple entities
In order to create test data definitions
As a user of Ditto
I want to load data entity definitions

Scenario: Ditto parses a single correct entity
  Given a file named "simple_entity.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
  }
  """
  When I run ditto on "simple_entity.ditto"
  Then the exit code should be 0
  And the stdout should be
  """
  checked 1 entities, 0 relationships, 0 errors

  """
  When I run ditto -v on "simple_entity.ditto"
  Then the exit code should be 0
  And the stdout should be
  """
  1 definition files loaded
  checking entities...
  currency
  checked 1 entities, 0 relationships, 0 errors

  """
  When I run ditto -vv on "simple_entity.ditto"
  Then the exit code should be 0
  And the stdout should be
  """
  loading file: simple_entity.ditto
  1 definition files loaded
  checking entities...
  currency
  checked 1 entities, 0 relationships, 0 errors

  """

Scenario: Ditto parses a faulty entity attribute
  Given a file named "faulty_attribute.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique, :radar ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
  }
  """
  When I run ditto on "faulty_attribute.ditto"
  Then the exit code should be 2
  And the stdout should be
  """
  currency: unknown properties: 'radar'
  checked 1 entities, 0 relationships, 1 errors

  """

Scenario: Ditto parses a correct entity relationship
  Given a file named "simple_relationship.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
    currency_group: :related
  }
  entity :currency_group, {
    :code => [ :mandatory, :unique ],
    :description => nil,
    :is_commodity => nil
  }
  """
  When I run ditto on "simple_relationship.ditto"
  Then the exit code should be 0
  And the stdout should be
  """
  checked 2 entities, 1 relationships, 0 errors

  """
Scenario: Ditto parses a bad entity relationship
  Given a file named "bad_relationship.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
    currency_band: :related
  }
  entity :currency_group, {
    :code => [ :mandatory, :unique ],
    :description => nil,
    :is_commodity => nil
  }
  """
  When I run ditto on "bad_relationship.ditto"
  Then the exit code should be 2
  And the stdout should be
  """
  currency: unknown relationship to currency_band
  checked 2 entities, 1 relationships, 1 errors

  """
Scenario: Ditto detects duplicate entity definitions
  Given a file named "simple_entity.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
  }
  """
  When I run ditto with the file "simple_entity.ditto" twice
  Then the exit code should be 2
  And the stdout should be
  """
  currency: duplicate definition of entity
  (was previously defined at simple_entity.ditto:1)
  checked 1 entities, 0 relationships, 1 errors

  """
