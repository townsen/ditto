@pending
Feature: Ditto stores entities
In order to create test data
As a user of Ditto
I want to load data

Scenario: Ditto detects missing mappings
  Given a file named "missing_mapping.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
  }
  """
  And a file named "missing_mapping.yaml" with:
  """
  version: 1.0.0
  date:    2012-10-13
  ---
  currency:
    code: GBP
    description: Pounds Sterling
    exchange_rate: 1.5
    is_designated: false
    currency_group: Europe
  """
  When I run ditto on two files "missing_mapping.ditto", "missing_mapping.yaml"
  Then the exit code should be 5
  And the stdout should be
  """
  checked 1 entities, 0 relationships, 0 errors
  validated 1 entities, 1 instances
  currency: no mapping

  """

Scenario: Ditto stores a single correct entity
  Given a file named "simple_entity.ditto" with:
  """
  entity :currency_group, {
    :code => [ :mandatory, :unique ],
    :description => nil,
    :is_commodity => nil
  }
  require 'dm/currency_group'
  require 'dm/currency'

  add :currency_group, :version => '1.0.0' do |ditto|
    CurrencyGroup.create(
      :code => ditto.code,
      :description => ditto.description,
      :is_commodity => ditto.is_commodity
    )
  end
  """
  And a file named "simple_entity.yaml" with:
  """
  version: 1.0.0
  date:    2012-10-13
  ---
  currency_group:
    code: Europe
    description: European Countries
    is_commodity: false
  """
  When I run ditto on two files "simple_entity.ditto", "simple_entity.yaml"
  Then the stdout should be
  """
  checked 1 entities, 0 relationships, 0 errors
  validated 1 entities, 1 instances
  stored 1 entities, 1 instances

  """
  And the exit code should be 0

Scenario: Ditto stores related entities
  Given a file named "simple_relationship.ditto" with:
  """
  entity :currency, {
    code:	    [ :mandatory, :unique ],
    description:    nil,
    exchange_rate:  :mandatory,
    is_designated:  nil,
    currency_group: :related
  }
  entity :currency_group, {
    :code => [ :mandatory, :unique ],
    :description => nil,
    :is_commodity => nil
  }
  require 'dm/currency_group'
  require 'dm/currency'

  add :currency_group, :version => '1.0.0' do |ditto|
    CurrencyGroup.create(
      :code => ditto.code,
      :description => ditto.description,
      :is_commodity => ditto.is_commodity
    )
  end
  add :currency, :version => '1.0.0' do |ditto|
    cg = CurrencyGroup.get(ditto.currency_group)
    cg.currencies.create(
      :code	=> ditto.code,
      :description => ditto.description,
      :exchange_rate => ditto.exchange_rate,
      :is_designated => ditto.is_designated
    )
  end

  """
  And a file named "simple_relationship.yaml" with:
  """
  version: 1.0.0
  date:    2012-10-13
  ---
  currency:
    code: GBP
    description: Pounds Sterling
    exchange_rate: 1.5
    is_designated: false
    currency_group: Europe
  ---
    currency_group:
      code: Europe
      description: European Countries
      is_commodity: false
  """
  When I run ditto on two files "simple_relationship.ditto", "simple_relationship.yaml"
  Then the stdout should be
  """
  checked 2 entities, 1 relationships, 0 errors
  validated 2 entities, 2 instances
  stored 2 entities, 2 instances

  """
  And the exit code should be 0
