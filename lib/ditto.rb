require 'ditto/version'
require 'ditto/options'
require 'ditto/entity'
require 'ditto/error'
require 'ditto/dsl'
include Ditto::DSL
module Ditto
  def self.symbolize_keys h
    h.inject({}) { |opts,(k,v)| opts[(k.to_sym rescue k) || k] = v; opts }
  end
end
