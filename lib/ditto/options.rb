require 'optparse'
require 'ostruct'
require 'singleton'

module Ditto
  class Options < OpenStruct

    DEFAULTS = {
	:connstring => ENV['DATABASE_URL'] || 'nodb://a/b',
	:entitydir  => 'ditto',
	:dittomart  => ENV['DITTO_MART'],
	:debug	    => false,
	:check	    => false,
	:droptables => false,
	:verbose    => 0,
	:loadfiles  => nil
    }
    def initialize argv
      super(DEFAULTS)

      Ditto::Options::DEFAULTS.each do |k,v|
	self.send "#{k}=".to_s, v
      end

      OptionParser.new do |opts|
	opts.banner = "Usage: ditto [ options ] files..."
	opts.separator "Files specified (in yaml format) will be loaded into database"
	opts.on("-c STRING", "--connection", String, "Database connection string",
		"Defaults to environment variable DATABASE_URL",
		"Current default is #{DEFAULTS[:connstring]}") do |s|
	  self.connstring = s
	end
	opts.on("-e DIR", "--entitydir", String, "Directory to load Ditto entities from (default #{DEFAULTS[:entitydir]})") do |m|
	  raise "Entity dir #{m} does not exist!" unless Dir.exist?(m)
	  self.entitydir = m
	end
	opts.on("-m FILE", "--dittomart", String, "File containing Datamapper models for target Datamart (default #{DEFAULTS[:dittomart]})") do |m|
	  self.dittomart = m
	end
	opts.on("-d", "--debug", "Debug") do |s|
	  self.debug = true
	end
	opts.on("-v", "--verbose", "Verbose (repeat for more)") do
	  self.verbose += 1
	end
	opts.on("--check", "Check given files, don't do anything!") do
	  self.check = true
	end
	opts.on("--droptables", "Delete existing database tables!") do
	  self.droptables = true
	end
	opts.on("-?", "-h", "--help", "Show this message") do
	  puts opts
	  exit
	end
	begin
	  opts.parse!(argv)
	rescue OptionParser::ParseError => e
	  STDERR.puts e.message, "\n", opts
	  exit(-1)
	end
      end
      self.martfiles = atfile self.dittomart
      self.loadfiles = argv.map{ |m| atfile m}.flatten
      return self
    end

    # Take a filename and expand it if it's an @file
    # relative paths are always relative to the including file
    # return an array of absolute filenames
    #
    def atfile fn, dir = '.'
      return [] unless fn
      af = fn.split('@',2)
      fn = af[1] if af.size == 2
      fn = File.expand_path(fn,dir)
      raise Errno::ENOENT, fn unless File.exist? fn
      return Array[fn] if af.size == 1
      dir = File.dirname File.absolute_path(fn)
      fh = File.open(fn)
      files = []
      fh.each_with_index do |line,ix|
	line.strip!
	begin
	  files.push *atfile(line,dir) unless line =~ /(^\s*$|^\s*#)/
	rescue
	  $!.message << " at '#{fn}:#{ix}'"
	  raise
	end
      end
      return files
    end

  end
end
