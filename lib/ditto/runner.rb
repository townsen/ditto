require 'yaml'
require 'ditto'
require 'data_mapper'

module Ditto
  class Runner
    def initialize(argv)
      @opts = Options.new(argv)
    end

    def run
      begin
	if @opts.check
	  puts "Checking..." if @opts.verbose > 0
	  load_files @opts.loadfiles
	  exit
	end
	puts "loading instances..." if @opts.verbose > 0
	load_files @opts.loadfiles, '.yaml'
	raise "dittomart file must be specified!" unless @opts.dittomart
	puts "loading dittomart: #{@opts.dittomart}" if @opts.verbose > 0
	load_files @opts.martfiles, '.dm'
	Ditto::Entity.load_entities @opts.entitydir, @opts.verbose
	store_data
	return 0
      rescue Ditto::Error => de
	STDERR.puts "\nError: #{de.message(@opts.debug)}"
      rescue StandardError => e
	STDERR.puts "\n#{e.class}: #{e.message}"
	STDERR.puts e.backtrace if @opts.debug
      end
      return 99
    end

    # Load any filetype depending on it's extension
    #
    def load_files list, type = nil
      nfiles = 0
      nerr = 0
      list.each do |f|
	ext = File.extname(f)
	next if type and ext != type
	print "loading #{f}" if @opts.verbose > 1
	nfiles += 1
	case ext
	when '.yaml'
	  nent = Entity.load_instances f, @opts.verbose
	  puts "#{nent} instances loaded" if @opts.verbose > 0
	when '.ditto'
	  Ditto::Entity.load_from_file f
	  puts "" if @opts.verbose > 1
	when '.dm'
	  begin
	    load File.absolute_path(f)
	  rescue ScriptError, StandardError => le
	    loc = le.backtrace[2].split(':')
	    puts "Error: #{le.to_s} (line #{loc[1]} in #{loc[0]})"
	  end
	  puts "" if @opts.verbose > 1
	end
      end
      puts "#{nfiles} files loaded" if @opts.verbose > 0
    end

    # Add the data to the database
    #
    def store_data
      puts "connecting to #{@opts.connstring}" if @opts.verbose > 0
      DataMapper::Logger.new(STDOUT, (@opts.verbose > 2) ? :debug : :info)
      DataMapper.setup(:default, @opts.connstring)
      DataMapper.finalize
      begin
	if @opts.droptables
	  DataMapper.auto_migrate!
	else
	  DataMapper.auto_upgrade!
	end
	return Ditto::Entity.store_all @opts.verbose
      rescue StandardError => e
	STDERR.puts "\nERROR: #{e.to_s}"
	STDERR.puts e.backtrace if @opts.debug
	return false
      end
    end
  end
end
