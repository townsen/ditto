module Ditto
  class Error < StandardError
    attr_reader :src
    def initialize(src = nil)
      @src = src
    end
    def source debug=false
      return "n/a" if @src.nil? or @src[0].nil? or @src[1].nil?
      @src[0] = File.basename(@src[0]) unless debug
      "#{@src[0]}:#{@src[1]}"
    end
    def message debug=false
      msg = "#{to_s}"
      msg += " at #{source(debug)}" unless src.nil?
      msg += "\n#{backtrace.join("\n")}" if debug
      return msg
    end
  end
end
