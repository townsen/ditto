# Entity support for Ditto
#
require 'ditto'
require 'ostruct'

module Ditto
  class Entity
    PROPS = [ :mandatory, :unique, :related ].freeze
    # Entities are stored in a hash keyed by entity name
    # The contents are an array of Entity objects one for each version
    @@entities = Hash.new {|h,k| h[k] = []}
    # Instances are stored in a hash keyed by entity name
    # The contents are a hash (keyed by version) of arrays of instance hashes
    @@instances = Hash.new {|h,k| h[k] = Hash.new{|i,j|i[j] = []}}

    def self.reset
      @@entities.clear
      @@instances.clear
    end

    attr_reader :name, :version, :fields, :deps, :loc, :methods

    # Maps to the 'entity' DSL keyword
    # Stack depth is just empirically determined! TODO WRITE A TEST!
    #
    def initialize name, version, fields, methods
      src = Thread.current.backtrace[3].split(':')[0..1]

      @name = name.to_sym
      @version = Gem::Version.new(version) rescue raise(Error.new(src), "#{name}: #{$!.message}")
      @methods = methods
      @loc = src
      @deps = []

      if @@entities.has_key?(@name) and @@entities[@name].any?{|e| e.version == @version}
	loc = @@entities[@name].select{|e| e.version == @version}.first.loc
	raise Error.new(src), "#{name}: previously defined at #{loc.join(':')}"
      end
      raise Error.new(src), "#{name}: entity fields must be a hash!" unless fields.kind_of? Hash

      @fields = Ditto.symbolize_keys fields

      nkey = 0
      @fields.each do |field, props|
	symprops = Array(props).select{|p| p.is_a? Symbol}
	duffprops = symprops - PROPS
	unless duffprops.empty?
	  raise Error.new(src), "#{name}: unknown properties: '#{duffprops.join(' ,')}'"
	end
	nkey += symprops.count(:unique)
	@deps << field if symprops.include? :related
      end

      @@entities[@name] << self
    end

    # Errors discovered in here will be load/syntax errors
    # Stack depth is just empirically determined! TODO WRITE A TEST!
    #
    def self.load_from_file f
      begin
	load File.absolute_path(f)
	return true
      rescue Ditto::Error
	raise
      rescue SyntaxError => se # Includes the failing location
	raise Error.new(), "#{se.class}: #{se.to_s}"
      rescue StandardError => le
	loc = le.backtrace[0].split(':')
	raise Error.new(loc), "#{f}: #{le.class}: #{le.to_s}"
      end
    end

    # Load data from YAML files into the in-memory representation
    # return number of instances loaded if all OK
    #
    def self.load_instances f, verbose = 0
      header = nil
      version = nil
      nent = 0
      YAML::load_documents(File.open(f)) do |doc|
	unless header
	  header = Ditto.symbolize_keys doc
	  version = Gem::Version.new(header[:version])
	  puts (verbose > 1) ? " (version: #{version.to_s})" : ""
	  puts "header #{header.to_s}" if verbose > 2
	  next
	end
	raise Error.new(), "instance has no version header" if version.nil?
	e = doc.flatten
	raise Error.new(), "instance malformed" if e.size != 2
	name = e[0].to_sym
	fields = Ditto.symbolize_keys e[1]
	puts "#{name}: #{fields.inspect}" if verbose > 2
	@@instances[name][version] << fields
	nent += 1
      end
      return nent
    end

    # Load entities required by the instances
    #
    def self.load_entities filepath, verbose = 0
      puts "loading entities..." if verbose > 0
      @@instances.each_key do |name|
	file = File.expand_path("#{name}.ditto", filepath)
	puts "loading #{file}" if verbose > 1
	self.load_from_file File.expand_path("#{name}.ditto", filepath)
	puts "#{name}: #{@@entities[name].inspect}" if verbose > 2
      end
      puts "loaded #{@@entities.size} entities" if verbose > 0
    end

    # Store the instances in dependency order
    #
    def self.store_all verbose = 0
      seq = []
      @@entities.each_key { |name| self.dep_list(name, seq, []) }
      puts "dependency sequence: #{seq.inspect}" if verbose > 2
      nsto = 0
      seq.each do |name|
	@@instances[name].each do |version, instances|
	  map = self.find_entity_map name, version, :add
	  instances.each do |instance|
	    puts "store #{name}: #{instance.inspect}" if verbose > 2
	    map[2].call OpenStruct.new(instance)
	    nsto += 1
	  end
	end
      end
      puts "#{nsto} instances stored" if verbose > 0
    end

    # Given the input data version get the right entity
    # We assume all entity versions with same major work
    # and take the highest
    #
    def self.find_entity_map name, dataversion, type
      list = @@entities[name].sort{|a,b| b.version <=> a.version}
      majorversion = dataversion.to_s.slice /^\d+\./
      entityreq = Gem::Requirement.new("~>#{majorversion}0")
      entity = list.find {|e| entityreq.satisfied_by? e.version }
      raise Error.new(),"#{name}: can't find entity matching dataversion #{dataversion.to_s}" unless entity

      # Search for the most recent map that matches the database
      # datamapper objects

      map = entity.methods.reverse.find do |m|
	m[0] == type and self.check_dm_versions(m[1])
      end
      raise Error.new(),"#{name}: can't find #{type} map that matches current database" unless map
      return map
    end

    def self.check_dm_versions requirements
      requirements.all? do |klass, req|
	loadedver = Gem::Version.new(eval "#{klass}::VERSION")
	req.satisfied_by?(loadedver)
      end
    end

    # Compute dependencies recursively
    # build a list in dependency sequence
    #
    def self.dep_list(name, list, done)
      raise "circular dependency: #{name}" if done.include? name
      raise "missing dependency: #{name}" unless @@entities.has_key? name
      deps = @@entities[name].flat_map {|e| e.deps}.uniq
      done.push name
      deps.each do |depname|
	self.dep_list depname, list, done
      end
      done.pop
      list << name unless list.include? name
    end
  end
end
