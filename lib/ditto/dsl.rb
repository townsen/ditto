# Define the DSL for Ditto
# Arguably the validation logic here should be back in entity.rb
#
module Ditto
  module DSL
    def entity (name, version, opts, *methods)
      unless methods.size > 0
	src = Thread.current.backtrace[1].split(':')[0..1]
	raise Error.new(src), "#{name}: entity missing map method!"
      end
      Ditto::Entity.new(name,version,opts,methods)
    end
    def add (version, &block)
      unless block
	src = Thread.current.backtrace[1].split(':')[0..1]
	raise Error.new(src), "add method missing block (use {} not do/end)"
      end
      [:add, check_versions(version), block]
    end
    def delete (version, &block)
      unless block
	src = Thread.current.backtrace[1].split(':')[0..1]
	raise Error.new(src), "delete method missing block (use {} not do/end)"
      end
      [:delete, check_versions(version), block]
    end
    # Check the version hash and convert targets to Gem::Requirements
    #
    def check_versions version
      begin
	version.each {|k,v| version[k] = Gem::Requirement.new(v)}
      rescue
	src = $!.backtrace[9].split(':')[0..1]
	raise Error.new(src), $!.message
      end
    end
  end
end

include Ditto::DSL
