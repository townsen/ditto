# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ditto/version'

Gem::Specification.new do |gem|
  gem.name        = 'ditto'
  gem.version     = Ditto::VERSION
  gem.date        = '2012-10-12'
  gem.summary     = "Database independent test objects"
  gem.description = <<DESC;
Ditto defines a simple DSL that allows test data to be expressed in a database independent format.When the underlying tables are remapped the QA team don't have to go and change all the historical test cases.
DESC
  gem.authors     = ["Nick Townsend"]
  gem.email       = 'nick.townsend@mac.com'
  gem.platform	= Gem::Platform::RUBY
  gem.required_ruby_version = '>=1.9'
  gem.files	= Dir['lib/**/*.rb','bin/*']
  gem.executables	= [ 'ditto' ]
  gem.test_files	= `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.homepage    = 'https://github.com/townsen/ditto'
  gem.add_runtime_dependency "data_mapper", [">= 1.2.0"]
  gem.add_development_dependency "rspec", [">= 2.11"]
  gem.require_paths = ['lib']
  gem.has_rdoc	= false
end
